import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule }    from '@angular/common/http';

import { OfferManagementModule } from './offer-management/offer-management.module';
import { OfferManagementRoutingModule } from './offer-management/offer-management-routing.module';

import 'hammerjs';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SortableModule } from 'ngx-bootstrap/sortable';

@NgModule({
  declarations: [
    AppComponent,
  ],
  entryComponents: [
  ],
  imports: [
    BrowserModule,
    OfferManagementModule,
    OfferManagementRoutingModule,
    HttpClientModule,

    ModalModule.forRoot(),
    SortableModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

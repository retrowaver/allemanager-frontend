import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuctionListComponent } from './components/auction-list/auction-list.component';
import { NewDraftComponent } from './components/new-draft/new-draft.component';
import { EditDraftComponent } from './components/edit-draft/edit-draft.component';
import { DeleteDraftComponent } from './components/delete-draft/delete-draft.component';


const routes: Routes = [
  { path: '', redirectTo: '/aukcje', pathMatch: 'full' },
  { path: 'aukcje', component: AuctionListComponent },
  { path: 'szkic/nowy', component: NewDraftComponent },
  { path: 'szkic/edycja/:id', component: EditDraftComponent },
  { path: 'szkic/usuwanie/:id', component: DeleteDraftComponent },
];

@NgModule({
  exports: [ RouterModule ],
  imports: [RouterModule.forRoot(routes) ]
})

export class OfferManagementRoutingModule {
}

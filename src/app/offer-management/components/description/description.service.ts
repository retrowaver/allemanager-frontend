import { Injectable } from '@angular/core';
import { Description, TextElement, ListElement } from './description';
import { OfferDescription, TextOfferItem, ImageOfferItem } from '../../interfaces/offer-description';

@Injectable({
  providedIn: 'root',
})
export class DescriptionService {
  getDescriptionPreview(description: Description) {
    return this.buildContent(description);
  }

  convertOfferDescriptionToDescription(offerDescription: OfferDescription): Description {
    if (offerDescription !== null) {
      return null;
    }
    return this.getEmptyDescription();
  }

  convertDescriptionToOfferDescription(description: Description, images: Array<{'url': string}>): OfferDescription {
    const content = this.buildContent(description);
    if (content === '') {
      return null;
    }

    const offerDescription: OfferDescription = {
      sections: [
        {
          items: [
            {
              type: 'TEXT',
              content: content
            }
          ]
        }
      ]
    };

    // Add first photo alongside content (on the left) in first section
    if (images[0] !== undefined) {
      offerDescription.sections[0].items.unshift(
        {
          type: 'IMAGE',
          url: images[0].url
        }
      );
    }

    // If there's just a single photo, then there's nothing more to do
    if (images.length < 2) {
      return offerDescription;
    }

    // If there are more photos, display them all in sections below (first photo is displayed once again)
    let index = 0;
    for (let image of images) {
      let sectionIndex = Math.floor(index / 2) + 1;

      if (index % 2 === 0) {
        offerDescription.sections[sectionIndex] = {items: []};
      }

      offerDescription.sections[sectionIndex].items[index % 2] = {
        type: 'IMAGE',
        url: image.url
      };

      index++;
    }

    return offerDescription;
  }

  protected buildContent(description: Description): string {
    let content = '';
    
    for (let element of description.elements) {
      if (element.type === 'text') {
        content = content.concat(this.getTextContent(element));
      } else if (element.type === 'list') {
        content = content.concat(this.getListContent(element));
      }
    }

    return content;
  }

  protected getTextContent(element: any): string {
    if (element.value === null || element.value.trim().length === 0) {
      return '';
    }

    return ''.concat(element.prefix, element.value, element.suffix);
  }

  protected getListContent(element: any): string {
    if (element.values.length === 0) {
      return '';
    }

    let rows = '';
    for (let value of element.values) {
      rows = rows.concat(element.rowPrefix, value.name, ': ', value.value, element.unit, element.rowSuffix);
    }

    return ''.concat(element.prefix, rows, element.suffix);
  }

  protected getEmptyDescription() {
    const description = new Description;

    description.elements.push(<TextElement>{
      id: 'title',
      type: 'text',
      displayedName: 'Tytuł',
      prefix: '<h1>',
      suffix: '</h1>',
      predefinedTextValues: [],
      value: null
    });

    description.elements.push(<TextElement>{
      id: 'condition_short',
      type: 'text',
      displayedName: 'Stan krótko',
      prefix: '<h2>',
      suffix: '</h2>',
      predefinedTextValues: ['Stan idealny', 'Stan bardzo dobry', 'Stan bardzo dobry z minusem za ', 'Nowy z metką', 'Nowy bez metki', 'Nowa z metką', 'Nowa bez metki'],
      value: null
    });

    description.elements.push(<TextElement>{
      id: 'basic_description',
      type: 'text',
      displayedName: 'Opis',
      prefix: '<p>',
      suffix: '</p>',
      predefinedTextValues: [],
      value: null
    });

    description.elements.push(<TextElement>{
      id: 'condition_long',
      type: 'text',
      displayedName: 'Stan długo',
      prefix: '<p>',
      suffix: '</p>',
      predefinedTextValues: [' wygląda jak nowy.', ' wygląda jak nowa.', ' w stanie bardzo dobrym.', ' w stanie bardzo dobrym z minusem za widoczne niewielkie ślady użytkowania.'],
      value: null
    });

    description.elements.push(<TextElement>{
      id: 'size',
      type: 'text',
      displayedName: 'Rozmiar z metki',
      prefix: '<p><b>',
      suffix: '</b></p>',
      predefinedTextValues: ['Rozmiar z metki ', 'Rozmiar z metki . Proszę dokładnie sprawdzić podane wymiary.'],
      value: null
    });

    description.elements.push(<TextElement>{
      id: 'washing',
      type: 'text',
      displayedName: 'Można prać w pralce',
      prefix: '<p>',
      suffix: '</p>',
      predefinedTextValues: ['Można prać w pralce.'],
      value: null
    });

    description.elements.push(<TextElement>{
      id: 'extra',
      type: 'text',
      displayedName: 'Dodatkowe informacje',
      prefix: '<p><b>',
      suffix: '</b></p>',
      predefinedTextValues: ['Otrzymasz pachnącą, wypraną i wyprasowaną odzież, gotową do założenia.'],
      value: null
    });

    description.elements.push(<ListElement>{
      id: 'measurements',
      type: 'list',
      displayedName: 'Wymiary',
      prefix: '<h2>Wymiary:</h2><ul>',
      suffix: '</ul>',

      rowPrefix: '<li>',
      rowSuffix: '</li>',
      predefinedListValues: [
        {
          category: 'Sukienki',
          values: [
            'Długość od ramienia',
            'Szerokość w paszkach'
          ]
        },
        {
          category: 'Kurtki / bluzy / bluzki',
          values: [
            'Długość od ramienia',
            'Długość rękawka od paszki',
            'Długość rękawka od ramienia',
            'Szerokość w paszkach'
          ]
        },
        {
          category: 'Spódniczki',
          values: [
            'Długość całkowita',
            'Szerokość w pasie'
          ]
        },
        {
          category: 'Wszystkie',
          values: [
            'Długość całkowita',
            'Długość nogawki wewnętrznej',
            'Długość nogawki zewnętrznej',
            'Długość od ramienia',
            'Długość od szyi',
            'Długość rękawka od paszki',
            'Długość rękawka od ramienia',
            'Długość rękawka od szyi',
            'Szerokość dołem',
            'Szerokość nogawki dołem',
            'Szerokość nogawki w kolanku',
            'Szerokość nogawki w kroku',
            'Szerokość w biodrach',
            'Szerokość w pasie',
            'Szerokość w paszkach'
          ]
        }
      ],
      predefinedListValuesSets: [
        {
          displayedName: 'Sukienki',
          values: [
            'Długość od ramienia',
            'Szerokość w paszkach'
          ]
        },
        {
          displayedName: 'Kurtki / bluzy / bluzki',
          values: [
            'Długość od ramienia',
            'Długość rękawka od paszki',
            'Długość rękawka od ramienia',
            'Szerokość w paszkach'
          ]
        },
        {
          displayedName: 'Spódniczki',
          values: [
            'Długość całkowita',
            'Szerokość w pasie'
          ]
        },
        {
          displayedName: 'Spodnie',
          values: [
            'Długość nogawki wewnętrznej',
            'Długość nogawki zewnętrznej',
            'Szerokość w pasie',
            'Szerokość w biodrach',
            'Szerokość nogawki dołem',
            'Szerokość nogawki w kolanku',
            'Szerokość nogawki w kroku'
          ]
        }
      ],
      values: [],
      unit: ' cm'
    });

    description.elements.push(<ListElement>{
      id: 'composition',
      type: 'list',
      displayedName: 'Skład materiału',
      prefix: '<h2>Skład materiału:</h2><ul>',
      suffix: '</ul>',

      rowPrefix: '<li>',
      rowSuffix: '</li>',
      predefinedListValues: [
        {
          category: 'Popularne',
          values: [
            'Bawełna',
            'Poliester',
            'Elastan',
            'Wiskoza',
            'Nylon',
            'Akryl'
          ]
        },
        {
          category: 'Pozostałe',
          values: [
            'Acetate',
            'Jedwab',
            'Kaszmir',
            'Len',
            'Metallised',
            'Moher',
            'Poliakryl',
            'Poliamid',
            'Rayon',
            'Spandex',
            'Wełna'
          ]
        },
        {
          category: 'Inne',
          values: [
            'Inne włókna',
            'Mieszane włókna'
          ]
        }
      ],
      predefinedListValuesSets: [],
      values: [],
      unit: '%'
    });

    return description;
  }
}

export class Description {
  elements: Element[] = [];
}

export interface Element {
  id: string;
  type: string;
  displayedName: string;
  prefix: string;
  suffix: string;
}
  
export class TextElement implements Element {
  id: string;
  type: string;
  displayedName: string;
  prefix: string;
  suffix: string;


  predefinedTextValues: string[];

  value: string;
}

export class ListElement implements Element {
  id: string;
  type: string;
  displayedName: string;
  prefix: string;
  suffix: string;


  rowPrefix: string;
  rowSuffix: string;

  predefinedListValues: Array<{
    category: string;
    values: string[];
  }>;

  predefinedListValuesSets: Array<{
    displayedName: string;
    values: string[];
  }>;

  newPredefinedListValue: string;
  newPredefinedListSetValue: string;
  values: Array<{name: string, value: string}>;

  unit: string;
}
import { Component, OnInit, Input } from '@angular/core';
import { Description } from './description';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent implements OnInit {

  @Input() description: Description;
  newListEntry: string;

  addValue(element: any, predefined: boolean) {
    element.values.push({
      name: predefined ? element.newPredefinedListValue : '',
      value: ''
    });
  }

  addValueSet(element: any) {
    let set = element.predefinedListValuesSets.find(set => set.displayedName === element.newPredefinedListSetValue);

    if (set === undefined) {
      return;
    }

    for (let value of set.values) {
      element.values.push({
        name: value,
        value: ''
      });
    }
  }

  removeValue(element: any, value: any) {
    const index = element.values.indexOf(value);
    element.values.splice(index, 1);
  }

  setPredefinedTextValue(element: any, predefinedValue: string) {
    element.value = predefinedValue;
  }

  constructor() { }

  ngOnInit() {
  }

}

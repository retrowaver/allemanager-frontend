import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OfferInfo } from '../../interfaces/offer-info';
import { OffersInfo } from '../../interfaces/offers-info';

import { PhpFriendlyQueryStringService } from '../../services/php-friendly-query-string.service';
import { NgControlStatus } from '@angular/forms';


import { catchError, map, tap } from 'rxjs/operators';
import { of, throwError } from 'rxjs';

import { BackendError } from '../../interfaces/backend-error';
import { BackendErrorService } from '../../services/backend-error.service';
import { DraftOffer } from '../../interfaces/draft-offer';


@Component({
  selector: 'app-auction-list',
  templateUrl: './auction-list.component.html',
  styleUrls: ['./auction-list.component.css']
})
export class AuctionListComponent implements OnInit {
  initialPageSize: number;
  pageSizeOptions: number[];
  totalCount: number;

  offers: OfferInfo[];

  displayedColumns: string[] = ['id', 'name', 'status', 'actions'];

  constructor(
    private http: HttpClient,
    private PFQS: PhpFriendlyQueryStringService,
    private backendErrorService : BackendErrorService
  ) { }

  ngOnInit() {
    this.totalCount = 1;
    this.initialPageSize = 5;
    this.pageSizeOptions = [5, 15, 50];

    this.updateOffers(this.initialPageSize, 0);
  }

  updateOffers(perPage: number, offset: number) {
    this.getOffers(perPage, offset).subscribe(
      response => {
        this.offers = response.offers;


        console.log(this.offers);


        this.totalCount = response.totalCount;
      },
      error => {
        alert(error.userMessage);
      }
    );
  }

  getOffers(perPage: number, offset: number) {
    const queryString = this.PFQS.getQueryString({
      'limit': perPage,
      'offset': offset,
      'publication.status': [
        'INACTIVE',
        'ACTIVE',
        'ACTIVATING'
      ]
    });

    return this.http.get<OffersInfo>(`http://localhost/sale/offers?${queryString}`, {withCredentials: true})
      .pipe(catchError(err => this.backendErrorService.handleCaughtError(err)));
  }

  publishOffer(offerId: string) {


    this.http.get<any>(`http://localhost/sale/offers/${offerId}`, {withCredentials: true})
      .pipe(catchError(err => this.backendErrorService.handleCaughtError(err)))
      .subscribe(
        offer => {
          if (offer.validation.errors.length > 0) {
            alert('TODO nie przechodzi walidacji');
            return;
          }

          if (offer.publication.status !== 'INACTIVE') {
            alert('TODO status inny niż INACTIVE');
            return;
          }

          const body = {
            offerCriteria: [
              {
                offers: [offerId],
                type: 'CONTAINS_OFFERS'
              }
            ],
            publication: {
              action: 'ACTIVATE'
            }
          };
      
      
          this.http.put<any>('http://localhost/sale/offer-publication-commands', body, {withCredentials: true})
          .pipe(catchError(err => this.backendErrorService.handleCaughtError(err)))
          .subscribe(
            response => console.log(response)
          );
        },
        error => alert(error.userMessage)
      );
  }

}

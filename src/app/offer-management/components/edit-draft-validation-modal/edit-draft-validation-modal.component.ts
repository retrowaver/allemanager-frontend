import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-edit-draft-validation-modal',
  templateUrl: './edit-draft-validation-modal.component.html',
  styleUrls: ['./edit-draft-validation-modal.component.css']
})
export class EditDraftValidationModalComponent implements OnInit {

  errors: [];
 
  constructor(public bsModalRef: BsModalRef) {}
 
  ngOnInit() {
  }
}

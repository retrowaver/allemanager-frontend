import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDraftValidationModalComponent } from './edit-draft-validation-modal.component';

describe('EditDraftValidationModalComponent', () => {
  let component: EditDraftValidationModalComponent;
  let fixture: ComponentFixture<EditDraftValidationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDraftValidationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDraftValidationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

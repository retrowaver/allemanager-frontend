import { Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { DraftOffer } from '../../interfaces/draft-offer';
import { Category } from '../../interfaces/category';
import { ParameterInfo } from '../../interfaces/parameter-info';
import { Parameter } from '../../interfaces/parameter';
import { OfferDescription } from '../../interfaces/offer-description';
import { Description } from '../description/description';
import { DescriptionService } from '../description/description.service';
import { tap, map } from 'rxjs/operators';
import { of, combineLatest } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { BackendErrorService } from '../../services/backend-error.service';

import { EditDraftValidationModalComponent } from '../edit-draft-validation-modal/edit-draft-validation-modal.component';

@Component({
  selector: 'app-edit-draft',
  templateUrl: './edit-draft.component.html',
  styleUrls: ['./edit-draft.component.css'],
  encapsulation: ViewEncapsulation.None //https://github.com/angular/angular/issues/7845#issuecomment-302326549
})

export class EditDraftComponent implements OnInit {
  offer: DraftOffer;

  categoryPickerCategories: Category[];
  selectedCategory: Category;
  categoryParameters: ParameterInfo[];
  description: Description;

  constructor(
    private http: HttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private descriptionService: DescriptionService,
    private modalService: BsModalService,
    private backendErrorService : BackendErrorService
  ) { }

  ngOnInit() {
    this.http.get<DraftOffer>(`http://localhost/sale/offers/${this.activatedRoute.snapshot.params['id']}`, {withCredentials: true})
      .pipe(catchError(err => this.backendErrorService.handleCaughtError(err)))
      .subscribe(
        offer => this.loadOffer(offer),
        error => alert(error.userMessage)
      );
  }

  loadOffer(offer) {
    this.offer = offer;

    this.selectCategory(this.offer.category !== null ? this.offer.category.id : null);
    this.description = this.descriptionService.convertOfferDescriptionToDescription(this.offer.description);


    console.log(this.offer);
    console.log(this.description);
  }

  selectCategory(categoryId: string) {
    combineLatest(
      this.getCategoryDetails(categoryId),
      this.getCategoryList(categoryId),
      this.getCategoryParameters(categoryId)
    ).subscribe(
      ([detailsRes, listRes, parametersRes]) => {
        this.offer.category = {id: categoryId};

        this.selectedCategory = detailsRes;

        this.categoryPickerCategories = listRes === null ? null : listRes.categories;

        this.categoryParameters = null;
        if (parametersRes !== null) {
          this.categoryParameters = parametersRes.parameters;
          this.addEmptyParameters(parametersRes.parameters);
        }
      },
      error => alert(error.userMessage)
    );

  }

  getCategoryDetails(categoryId: string) {
    if (categoryId === null) {
      return of(null);
    }

    return this.http.get<Category>(`http://localhost/sale/categories/${categoryId}`, {withCredentials: true})
      .pipe(catchError(err => this.backendErrorService.handleCaughtError(err)));
  }

  getCategoryList(categoryId: string) {
    const params = categoryId !== null ? {'parent.id': categoryId} : {};

    return this.http.get<{categories: Category[]}>(`http://localhost/sale/categories`, {params: params, withCredentials: true})
      .pipe(catchError(err => this.backendErrorService.handleCaughtError(err)));
  }

  getCategoryParameters(categoryId: string) {
    if (categoryId === null) {
      return of(null);
    }

    return this.http.get<{parameters: ParameterInfo[]}>(`http://localhost/sale/categories/${categoryId}/parameters`, {withCredentials: true})
    .pipe(catchError(err => this.backendErrorService.handleCaughtError(err)));
  }

  addEmptyParameters(categoryParameters: ParameterInfo[]) {
    for (let parameterInfo of categoryParameters) {
      if (this.offer.parameters.find(parameter => parameter.id == parameterInfo.id) !== undefined) {
        continue;
      }

      const parameter = new Parameter;
      parameter.id = parameterInfo.id;

      this.offer.parameters.push(parameter);
    }
  }

  getMatchingParameterInfo(parameter: Parameter): ParameterInfo {
    return this.categoryParameters.find(parameterInfo => parameterInfo.id === parameter.id);
  }

  save() {
    const offer = Object.assign({}, this.offer);
    offer.parameters = offer.parameters.filter(
      parameter => {
        // Exclude parameters from other categories
        if (this.categoryParameters.find(parameterInfo => parameterInfo.id === parameter.id) === undefined) {
          return false;
        }
        // Exclude parameters without values
        if (!parameter.values.length && !parameter.valuesIds.length) {
          //TODO if ever range values support will be added, there has to be another condition
          return false;
        }

        return true;
      }
    );

    if (this.description !== null) {
      offer.description = this.descriptionService.convertDescriptionToOfferDescription(this.description, this.offer.images);
    }

    console.log(offer);

    this.http.put<DraftOffer>(`http://localhost/sale/offers/${this.offer.id}`, offer, {withCredentials: true})
      .pipe(catchError(err => this.backendErrorService.handleCaughtError(err)))
      .subscribe(
        (draft: DraftOffer) => {
          this.offer.validation = draft.validation;

          if (this.offer.validation.errors.length === 0) {
            this.router.navigateByUrl('/aukcje');
          } else {
            this.modalService.show(EditDraftValidationModalComponent, {initialState: {errors: this.offer.validation.errors}});
          }
        },
        error => alert(error.userMessage)
      );
  }

  getAllegroTitleLength(title: string): number {
    let length = 0;

    if (title === null) {
      return length;
    }

    for (const c of title) {
      if (c === '<' || c === '>') {
        length += 4;
      } else if (c === '&') {
        length += 5;
      } else if (c === '"') {
        length += 6;
      } else {
        length++;
      }
    }

    return length;
  }
}

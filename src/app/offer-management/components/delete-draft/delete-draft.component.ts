import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-delete-draft',
  templateUrl: './delete-draft.component.html',
  styleUrls: ['./delete-draft.component.css']
})
export class DeleteDraftComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.http.delete(`http://localhost/sale/offers/${this.activatedRoute.snapshot.params['id']}`, {withCredentials: true}).subscribe(
      response => console.log(response)
    );
  }

}

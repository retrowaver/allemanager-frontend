import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteDraftComponent } from './delete-draft.component';

describe('DeleteDraftComponent', () => {
  let component: DeleteDraftComponent;
  let fixture: ComponentFixture<DeleteDraftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteDraftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDraftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from  '@angular/common/http';
import { map, tap, last, catchError } from  'rxjs/operators';
//import { getEventMessage,  } from 'rxjs';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ImagesComponent implements OnInit {

  @Input() offer: any;

  uploadingCount = 0;

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  fileChanged(event) {
    let formData;

    for (let file of event.target.files) {
      this.uploadingCount++;


      formData = new FormData();
      formData.append(file.name, file, file.name);

      this.http.post<any>('http://localhost/sale/images', formData, {
        withCredentials: true
      }).subscribe(
        response => {
          if (response.location === undefined) {
            return;
          }
          this.offer.images.push({url: response.location});
          this.uploadingCount--;

          // Reassign to trigger some stuff in sortable that refreshes it
          this.offer.images = Object.assign([], this.offer.images);
        },
        error => {
          this.uploadingCount--;
        }
      );
    }
  }

  removeImage(image) {
    const index = this.offer.images.indexOf(image);
    this.offer.images.splice(index, 1);

    // Reassign to trigger some stuff in sortable that refreshes it
    this.offer.images = Object.assign([], this.offer.images);
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { Parameter } from '../../../interfaces/parameter';
import { ParameterInfo } from '../../../interfaces/parameter-info';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css']
})
export class SelectComponent implements OnInit {

  @Input() parameter: Parameter;
  @Input() parameterInfo: ParameterInfo;

  constructor() { }

  ngOnInit() {
  }

}

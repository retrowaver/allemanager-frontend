import { Component, OnInit, Input } from '@angular/core';
import { Parameter } from '../../../interfaces/parameter';
import { ParameterInfo } from '../../../interfaces/parameter-info';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

  @Input() parameter: Parameter;
  @Input() parameterInfo: ParameterInfo;

  constructor() { }

  ngOnInit() {
  }

  onChange(id: string) {
    if (!this.hasId(id)) {
      this.parameter.valuesIds.push(id);
      return;
    }

    const index = this.parameter.valuesIds.indexOf(id);
    this.parameter.valuesIds.splice(index, 1);
  }

  hasId(id: string) {
    return this.parameter.valuesIds.includes(id);
  }

}

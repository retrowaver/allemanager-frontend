import { Component, OnInit, Input } from '@angular/core';
import { Parameter } from '../../../interfaces/parameter';
import { ParameterInfo } from '../../../interfaces/parameter-info';

@Component({
  selector: 'app-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.css']
})
export class NumberComponent implements OnInit {

  @Input() parameter: Parameter;
  @Input() parameterInfo: ParameterInfo;

  constructor() { }

  ngOnInit() {
  }

}

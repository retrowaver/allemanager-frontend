import { Component, OnInit, Input } from '@angular/core';
import { Parameter } from '../../interfaces/parameter';
import { ParameterInfo } from '../../interfaces/parameter-info';

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.css']
})
export class ParameterComponent implements OnInit {

  @Input() parameter: Parameter;
  @Input() parameterInfo: ParameterInfo;

  constructor() { }

  ngOnInit() {
  }

  getParameterControlType() {
    if (this.parameterInfo.type === 'dictionary' && this.parameterInfo.restrictions.multipleChoices === false) {
      return 'select';
    }

    if (this.parameterInfo.type === 'dictionary' && this.parameterInfo.restrictions.multipleChoices === true) {
      return 'checkbox';
    }

    if (this.parameterInfo.type === 'integer' || this.parameterInfo.type === 'float') {
      if (this.parameterInfo.restrictions.range) {
        //TODO (numer-range?) //Issue #6
      } else {
        return 'number';
      }
    }

    //TODO allowedNumberOfValues > 1
    if (this.parameterInfo.type === 'string' && this.parameterInfo.restrictions.allowedNumberOfValues === 1) {
      return 'string';
    }

    return '';
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { Parameter } from '../../../interfaces/parameter';
import { ParameterInfo } from '../../../interfaces/parameter-info';

@Component({
  selector: 'app-string',
  templateUrl: './string.component.html',
  styleUrls: ['./string.component.css']
})
export class StringComponent implements OnInit {

  @Input() parameter: Parameter;
  @Input() parameterInfo: ParameterInfo;

  constructor() { }

  ngOnInit() {
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { DraftOffer } from '../../interfaces/draft-offer';

@Component({
  selector: 'app-new-draft',
  templateUrl: './new-draft.component.html',
  styleUrls: ['./new-draft.component.css']
})
export class NewDraftComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    //let draft = <DraftOffer>{name: 'bez nazwy'};
    //let draft = <DraftOffer>{name: 'bez nazwy', category: {id: '257760'}};

    this.http.get<DraftOffer>('http://localhost/initialdraft', {withCredentials: true})
    .subscribe(
      draft => {
        this.http.post<DraftOffer>('http://localhost/sale/offers', draft, {withCredentials: true}).subscribe(
          draft => this.router.navigateByUrl(`/szkic/edycja/${draft.id}`)
          //(draft: DraftOffer) => console.log(draft)
        );
      }
    );
  }
}

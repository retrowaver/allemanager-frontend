export interface BackendError {
  status: number;
  userMessage: string;
}
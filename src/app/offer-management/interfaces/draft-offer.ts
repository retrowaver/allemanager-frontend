import { Parameter } from './parameter';
import { OfferDescription } from './offer-description';

export interface DraftOffer {
  name: string;
  id: string;
  category: {id: string};
  parameters: Array<Parameter>;
  description: OfferDescription;
  images: Array<{url: string}>;
  validation: any;
}
export interface OfferInfo {
  id: number;
  name: string;
  publication: {status: string};
}
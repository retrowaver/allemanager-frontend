import { OfferInfo } from './offer-info';

export interface OffersInfo {
  offers: OfferInfo[];
  totalCount: number;
}
export class Parameter {
  id: string = null;
  rangeValue: {
    from: string;
    to: string;
  } = null;
  values: string[] = [];
  valuesIds: string[] = [];
}
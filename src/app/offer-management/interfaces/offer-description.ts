export interface OfferDescription {
  sections: Array<{
    //items: ImageOfferItem[]|TextOfferItem[]
    items: any[]
  }>;
}

export interface ImageOfferItem {
  type: string;
  url: string;
}

export interface TextOfferItem {
  type: string;
  content: string;
}
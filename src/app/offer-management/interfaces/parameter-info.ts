export interface ParameterInfo {
  id: string;
  name: string;
  type: string;
  required: boolean;
  unit: string;
  options: {};
  restrictions: {
    multipleChoices: boolean;
    range: boolean;
    allowedNumberOfValues: number;
  };
  dictionary: object[];
}
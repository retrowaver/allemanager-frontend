export interface Category {
  id: string;
  leaf: boolean;
  name: string;
  parent: {id: string};
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewDraftComponent } from './components/new-draft/new-draft.component';
import { AuctionListComponent } from './components/auction-list/auction-list.component';
import { EditDraftComponent } from './components/edit-draft/edit-draft.component';
import { DeleteDraftComponent } from './components/delete-draft/delete-draft.component';
import { ParameterComponent } from './components/parameter/parameter.component';
import { SelectComponent } from './components/parameter/select/select.component';
import { CheckboxComponent } from './components/parameter/checkbox/checkbox.component';
import { StringComponent } from './components/parameter/string/string.component';
import { NumberComponent } from './components/parameter/number/number.component';
import { DescriptionComponent } from './components/description/description.component';
import { ImagesComponent } from './components/images/images.component';
import { EditDraftValidationModalComponent } from './components/edit-draft-validation-modal/edit-draft-validation-modal.component';

import { FormsModule } from '@angular/forms';

import { ModalModule } from 'ngx-bootstrap/modal';
import { SortableModule } from 'ngx-bootstrap/sortable';

import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AuctionListComponent,
    NewDraftComponent,
    EditDraftComponent,
    DeleteDraftComponent,
    ParameterComponent,
    SelectComponent,
    CheckboxComponent,
    NumberComponent,
    StringComponent,
    DescriptionComponent,
    ImagesComponent,
    EditDraftValidationModalComponent
  ], 
  entryComponents: [
    EditDraftValidationModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,



    ModalModule,
    SortableModule,
    RouterModule

  ],
  exports: [
    AuctionListComponent
  ]
})
export class OfferManagementModule { }

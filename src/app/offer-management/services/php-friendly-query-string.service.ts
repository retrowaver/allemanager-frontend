import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PhpFriendlyQueryStringService {

  constructor() { }

  getQueryString(parameters: object) {
    let queryParams = [];

    Object.keys(parameters).forEach((key:string)=>{
      if (Array.isArray(parameters[key])) {
        for (let element of parameters[key]) {
          queryParams.push(
            `${encodeURI(key)}[]=${encodeURI(element)}`
          );
        }
      } else {
        queryParams.push(
          `${encodeURI(key)}=${encodeURI(parameters[key])}`
        );
      }
    });

    return queryParams.join('&');
  }
}

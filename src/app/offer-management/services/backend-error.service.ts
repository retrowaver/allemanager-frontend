import { Injectable } from '@angular/core';
import { BackendError } from '../interfaces/backend-error';
import { catchError, map, tap } from 'rxjs/operators';
import { of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BackendErrorService {

  constructor() { }

  handleCaughtError(err) {
    const errorMessage = (err.error.error.message !== undefined) ? err.error.error.message : 'Coś poszło nie tak';

    const error: BackendError = {
      status: err.status,
      userMessage: errorMessage
    };

    return throwError(error);
  }
}

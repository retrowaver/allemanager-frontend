import { TestBed } from '@angular/core/testing';

import { PhpFriendlyQueryStringService } from './php-friendly-query-string.service';

describe('PhpFriendlyQueryStringService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PhpFriendlyQueryStringService = TestBed.get(PhpFriendlyQueryStringService);
    expect(service).toBeTruthy();
  });
});

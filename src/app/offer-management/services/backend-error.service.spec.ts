import { TestBed } from '@angular/core/testing';

import { BackendErrorService } from './backend-error.service';

describe('BackendErrorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackendErrorService = TestBed.get(BackendErrorService);
    expect(service).toBeTruthy();
  });
});
